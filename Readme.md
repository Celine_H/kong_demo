# Spring Boot, POSTGRES, JPA, Hibernate Rest API 

The app defines following CRUD APIs.

    GET /notes
    
    POST /notes
    
    GET /notes/{noteId}
    
    PUT /notes/{noteId}
    
    DELETE /notes/{noteId}

You can test them using postman or any other rest client.

## docker-compose up to route from kong to services which are defined in kong.yaml on particular paths

## define db credentials in application.properties


## curl -X GET http://localhost:8001/consumers/user123/jwt in terminal to get key and secret of user123

## go to https://jwt.io/

header -->  {
         "alg": "HS256",
         "typ": "JWT"
             }
             
payload --> {
        "iss": your_key
             }
             
verify signature --> {

          your_secret
            }

##take generated token and paste in postman when sending GET request to /notes path. you will be authorized.